import axios from 'axios';
import React from 'react';
import './App.css';
import { i18nKeys } from './keys';


function App() {
  // FIXME these variables could be improved
  const isLoggedIn = false;
  const email = "";
  const password = "";
  const error =  "";

  // event handlers

  const submitHandler = (event: any) => {
    // TODO login the user
    axios.post("/api/login");
    // TODO handle success and errors
    // if there is an error it should be displayed under the login form in the jsx
    // use the following key as the error message: i18nKeys.login.form.error
  }

  const logoutHandler = () => {
    // TODO handle logout
  }

  return (
    <div className="App">
      <div className='App-content'>
        <h2>{i18nKeys.general.title}</h2>

        {!isLoggedIn && (
          <>
            <form onSubmit={submitHandler}>
              <div className='Form'>
                <div className='Input'>
                  <label htmlFor="email">{i18nKeys.login.form.input.email}</label>

                  {/* TODO create an email input */}
                  <input
                    id="email"
                    />
                </div>

                <div className='Input'>
                  <label htmlFor="password">{i18nKeys.login.form.input.password}</label>

                  {/* TODO create a password input */}
                  <input
                    id="password"
                    />
                </div>

                <input type="submit" value={i18nKeys.login.form.submit} />
              </div>
            </form>

            <p className='Form-error'>
              {error}
            </p>
          </>
        )}

        {/* TODO handle logout by clicking on the following button */}
        {isLoggedIn && <button>{i18nKeys.button.logout}</button>}
      </div>
    </div>
  );
}

export default App;

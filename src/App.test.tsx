import React from "react";
import { render, screen, fireEvent, act } from "@testing-library/react";
import App from "./App";
import { i18nKeys } from "./keys";
import { CORRECT_EMAIL, CORRECT_PASSWORD } from "./constants";
import "@testing-library/jest-dom";

test("renders login page", () => {
  render(<App />);

  const loginTitle = screen.getByText(i18nKeys.general.title);
  expect(loginTitle).toBeInTheDocument();

  const emailInput = screen.getByText(i18nKeys.login.form.input.email);
  expect(emailInput).toBeInTheDocument();

  const passwordInput = screen.getByText(i18nKeys.login.form.input.password);
  expect(passwordInput).toBeInTheDocument();

  const submitButton = screen.getByText(i18nKeys.login.form.submit);
  expect(submitButton).toBeInTheDocument();
});

test("can enter informations and login", async () => {
  render(<App />);

  const emailInput: any = screen.getByLabelText(
    i18nKeys.login.form.input.email
  );
  act(() => {
    fireEvent.input(emailInput, { target: { value: CORRECT_EMAIL } });
  });
  expect(emailInput.value).toBe(CORRECT_EMAIL);

  const passwordInput: any = screen.getByLabelText(
    i18nKeys.login.form.input.password
  );
  act(() => {
    fireEvent.input(passwordInput, { target: { value: CORRECT_PASSWORD } });
  });
  expect(passwordInput.value).toBe(CORRECT_PASSWORD);

  await act(() => {
    fireEvent.click(screen.getByDisplayValue(i18nKeys.login.form.submit));
  });

  const logoutButton = screen.getByText(i18nKeys.button.logout);
  expect(logoutButton).toBeInTheDocument();
});

test("handles login error", () => {
  render(<App />);

  const emailInput: any = screen.getByLabelText(
    i18nKeys.login.form.input.email
  );
  fireEvent.input(emailInput, { target: { value: "wrong@email.com" } });

  const passwordInput: any = screen.getByLabelText(
    i18nKeys.login.form.input.password
  );
  fireEvent.input(passwordInput, { target: { value: "wrongpassword" } });

  fireEvent.click(screen.getByDisplayValue(i18nKeys.login.form.submit));

  const errorMessage = screen.getByText(i18nKeys.login.form.error);
  expect(errorMessage).toBeInTheDocument();
});

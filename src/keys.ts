export const i18nKeys = {
    general: {
        title: 'Hello Masana !',
    },
    button: {
        logout: 'Logout'
    },
    login: {
        form: {
            submit: 'Login',
            error: 'Login failed: Email or password is incorrect, please retry.',
            input: {
                email: 'Email',
                password: 'Password',
            },
        }
    }
}
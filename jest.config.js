module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
  transform: {
    "^.+\\.[tj]sx?$": "ts-jest",
    "\\.(css)$": "jest-css-modules-transform",
  },
  testEnvironment: "jsdom",
};

# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

---

## To do

### First step

Your goal is to implement a simple login flow.\
The login is handled by a mocked api powered by a service worker, see the available endpoints in `src/msw/authentification.msw.ts`.\
When the user is logged in, the page content will change to a logout button.\
Pressing the button will switch the display to the login form.

> Go further with [react-hook-form](https://react-hook-form.com/) to handle forms

### Second step

You will see that we don't validate anything, however we should make the email and password inputs required to submit the form.\
When an error is triggered, simply display the error under the corresponding field.\

> You can use [yup](https://github.com/jquense/yup) to handle field validation

### Third step

We use classic css files to handle styling, switch to [css-in-js](https://css-tricks.com/a-thorough-analysis-of-css-in-js/) instead.\

> You can use [styled-components](https://styled-components.com/docs/basics#getting-started)

### Bonus

- Use [react-query](https://react-query.tanstack.com/) to handle the api
- Use [react-router](https://reactrouter.com/en/main) to handle public (login page) and private routes (homepage)

You can also improve the app however you see fit, have fun 👍
